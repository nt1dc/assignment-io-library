section .text

%define STDOUT 1
%define STDIN 0

%define READ_CALL 0
%define WRITE_CALL 1
%define EXIT_CALL 60

%define SPACE 0x20
%define NEW_LINE 0xA
%define TAB 0x9

%define ASCII_0 0x30

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CALL      ; кладем код выхода в rax
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ;обнуляй
.loop:
    cmp byte[rax+rdi], 0    ; сравнивает текущее значение с 0
    je .end                 ; если 0, то выход
    inc rax                 ; если мы еще не вышли (символ не равен нулю), то увеличиваем аккумулятор на 1, 
    jmp .loop               ; повторяем цикл 
.end:
    ret                     ; возврат из функции

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
;по сути заимствуем код и 3 семинара 
print_string:
    call string_length
    mov rdx, rax
    mov rax, WRITE_CALL
    mov rsi, rdi
    mov rdi, STDOUT 
    push rcx      
    syscall  
    pop rcx      
    ret

    
; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ;кладем полученный код символа в стек
    mov rdi, rsp;
    call print_string;
    pop rdi             ;  забираем значение из стека
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp             ; адрес возврата
    push 0                  ; знак возврата
    mov rax, rdi
    mov rcx, 10
    .loop:
        xor rdx, rdx        ; тут текущая цифра будет
        dec rsp
        div rcx             
        add rdx, ASCII_0
        mov [rsp], dl       ; младший байт rdx
        test rax, rax       ; оставшееся число
        jnz .loop
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9

    .loop:
        mov r8b, [rdi + rax]
        mov r9b, [rsi + rax]
        cmp r8b, r9b
        jnz .end
        inc rax
        cmp r8, 0
        jnz .loop
        mov rax, 1
        ret
    .end:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdx, 1
    mov rax, READ_CALL
    mov rsi, rsp
    mov rdi, STDIN
    push rcx 
    syscall
    pop rcx
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r8
    push r9
    push rbx                       
    xor rbx, rbx 
    mov r8, rsi                     ; размер
    mov r9, rdi                     ; адрес


    .skip:                          
        call read_char
        test rax, rax               
        jz .success                
        mov byte[r9 + rbx], al      
        cmp byte[r9 + rbx], SPACE    
        je .skip
        cmp byte[r9 + rbx], TAB   
        je .skip
        cmp byte[r9 + rbx], NEW_LINE     
        je .skip
        inc rbx                    

    .read:
        call read_char
        cmp al, 0x21               
        jl .success
        mov byte[r9 + rbx], al    
        cmp r8, rbx                
        jl .error
        inc rbx                   
        jmp .read

    .success:
        mov rdx, rbx                
        mov rax, r9                
        mov byte[r9 + rbx], 0      
        pop rbx
        pop r9
        pop r8
        ret

    .error:
        xor rax, rax              
        pop rbx
        pop r9
        pop r8
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8              ; текущий символ
    xor rcx, rcx            ; длины
    mov r10, 10
    xor rax, rax            

    .loop:
        mov r8b, [rdi]
        inc rdi
        cmp r8b, '0'
        jb .end
        cmp r8b, '9'
        ja .end
        inc rcx
        mul r10
        sub r8b, '0'
        add rax, r8
        jmp .loop

    .end:
        mov rdx, rcx
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    jmp parse_uint  
                   
    .negative:
        inc rdi                     
        call parse_uint            
        inc rdx                    
        neg rax                    
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
                            ;сначала проверим поместиться ли строка вообще в буфер o.O
    xor r8, r8
    xor rax, rax
    call string_length      ; узнаем длину строки 
    inc rax                 ; не забудем прибавить лултерминал 
    cmp rdx, rax            ; сравним длунну полученной строки с длиной буфера
    jl .error
.loop:
    mov cl, byte[rdi+r8]
    mov byte[rsi + r8], cl 
    inc r8
    cmp r8, rax
    jl .loop
.end:
    dec rax                 ;мы увеличивали rax на 1, тк нужно было учитывать лултерминал, поэтому нужно отнять 1 
    ret
.error:
    mov rax, 0               ;  при ситуации если длина буфера меньше длины строки запишем в rax-0, 
                            ;  чтобы дальше ничего не писать в буфер, ибо смысл 
                            ;  записывать строку которая не поместиться  
    ret
